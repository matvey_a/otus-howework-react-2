export interface ChatUser {
    userName: string;
    connectedAt: Date;
}