import { configureStore } from '@reduxjs/toolkit';
import connectedUsersReducer from './stateMangment/connectedUsersReducer';
import messagesReducer from './stateMangment/messagesReducer';

const store= configureStore({
    reducer: {
        messages: messagesReducer,
        connectedUsers: connectedUsersReducer  
    },
   
});
export default store;