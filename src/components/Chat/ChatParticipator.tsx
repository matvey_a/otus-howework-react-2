import React from "react";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Actions } from "../../stateMangment/messagesReducer";
import { Actions as ActionsConnected } from "../../stateMangment/connectedUsersReducer";
import { Button, Snackbar } from "@mui/material";

interface Props {
    userName: string;
}

const ChatParticipator = (props: Props) => {
    const { userName } = props;
    const dispatch = useDispatch();
    
    const [textInput, setTextInput] = React.useState('');
    const [open, setOpen] = React.useState(false);

    const handleClose = (event: React.SyntheticEvent | Event, reason?: string) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    const handleInputChange = (event: { target: { value: React.SetStateAction<string>; }; }) => {
        setTextInput(event.target.value);
    }

    let listOfConnectedUsers: string[] = useSelector((x: any) => x.connectedUsers.list);

    const handleClickSend = () => {        
        if(listOfConnectedUsers.find(x => x == userName) )
        {
            dispatch(Actions.addMesage({sender:userName, text:textInput}));
            setTextInput('');
        }
        else
        {
             setOpen(true);
        } 
    }

    const handleConnect = () => {        
        dispatch(ActionsConnected.userConnected(userName));
    }

    const handleDisconnect = () => {        
        dispatch(ActionsConnected.userDisconnected(userName));
    }    

    const css = {
        margin: '11px',       
    };
    
    return <div>
        Chat Participator {userName}
        {/* <Button variant="outlined">Text</Button> */}
        <Button style={css} variant="outlined" onClick={handleConnect}>Подключиться</Button>
        <Button style={css} variant="outlined" onClick={handleDisconnect}>Отключиться</Button>        
        <input type="text" value={textInput} onChange={handleInputChange}  id="qwfqwfq" name="inp"></input> <Button variant="contained" onClick={handleClickSend}>Отправить</Button>
        <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={handleClose}
        message="Please Connect First"
        // action={action}
        />
    </div>;
}

export default ChatParticipator;