
import { useEffect, useState } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router";
import { ChatUser } from "../../models/chatUser";
import { Message } from "../../models/message";
import { Actions } from "../../stateMangment/messagesReducer";
import { Actions  as ActionsConnected  } from "../../stateMangment/connectedUsersReducer";

// interface Props {
//     connectedUsers: ChatUser[];
// }

const ChatRoom = () => {
   
    const fakeUser: ChatUser = {connectedAt: new Date(), userName: 'Fake User'};
    const connectedUsers: ChatUser[] = [fakeUser];
    const chatMessages: string[] = ['Fake message from UFO', 'Hello React World'];
    
    const dispatch = useDispatch();
    
    const [fixedInt, setFixedInt] = useState(0);    

    useEffect(() => {
        chatMessages.forEach(a => {
           if(listOfMessages.length == 0)
            {
                dispatch(Actions.addMesage({text: a, sender: fakeUser.userName}));                
            }            
        });

        if(listOfConnectedUsers.length == 0)
        {
            dispatch(ActionsConnected.userConnected(fakeUser.userName));
        }

    }, [fixedInt]);

    let listOfMessages: Message[] = useSelector((x: any) => x.messages.list);

    let listOfConnectedUsers: string[] = useSelector((x: any) => x.connectedUsers.list);

    const css = {margin: '22px' };
    
    return <div style={css}>
         <div>Connected users:
         {listOfConnectedUsers.map(x => <div>Name : {x}</div> ) }
         </div>
         <br/   >
         <div>Messages:         
         {listOfMessages.map(y => <div>{y.sender} : {y.text}</div> ) }
         </div>
    </div>;

}
export default ChatRoom;