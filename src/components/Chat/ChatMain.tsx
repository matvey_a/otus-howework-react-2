
import { connect, useSelector } from "react-redux";
import { useParams } from "react-router";
import { ChatUser } from "../../models/chatUser";
import ChatParticipator from "./ChatParticipator";
import ChatRoom from "./ChatRoom";

const ChatMain = () => {
       
    return <div >
        <ChatParticipator userName='User1'></ChatParticipator>
        <ChatParticipator userName='User2'></ChatParticipator>
        <br/>
        <ChatRoom/>        
    </div>;

}
export default ChatMain;