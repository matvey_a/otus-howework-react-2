export const connnectedUsersActionTypes = Object.freeze({
    connect: 'connectedUsers/connectd',
    disconnect: 'connectedUsers/disconnect',
});

export const Actions = Object.freeze({
    userConnected: (userName: string) => ({ type: connnectedUsersActionTypes.connect, payload: userName }),
    userDisconnected: (userName: string) => ({ type: connnectedUsersActionTypes.disconnect, payload: userName })
});

interface ConnectedUsersState {
    list: string[];
}

const initialState: ConnectedUsersState = {
    list: [],    
}

const connectedUsersReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case connnectedUsersActionTypes.connect:    
           
            const list:string[] = [...state.list];
            const item = action.payload;
            const index = list.findIndex(x => x === item);       
            if (index === -1) {
                list.push(item);
            }                
            const newState = { ...state, list: [...list] };
            return newState;
        case connnectedUsersActionTypes.disconnect:    
        
            const listOfConnected:string[] = [...state.list];
            const itemDisconnected = action.payload;
            const indexOfFound = listOfConnected.findIndex(x => x === itemDisconnected);       
            if (indexOfFound != -1) {
                listOfConnected.splice(indexOfFound, 1);
            }                
            const newStateAfterDisconnect = { ...state, list: [...listOfConnected] };            
            return newStateAfterDisconnect;               
        default:
            return state;
    }
};
export default connectedUsersReducer;