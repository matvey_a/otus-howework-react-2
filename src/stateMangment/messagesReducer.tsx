import { Message } from "../models/message";

export const messagesActionTypes = Object.freeze({
    addMessage: 'messages/add',
});

export const Actions = Object.freeze({
    addMesage: (message: Message) => ({ type: messagesActionTypes.addMessage, payload: message })
});

interface MessagesState {
    list: Message[];
}

const initialState: MessagesState = {
    list: [],    
}

const messagesReducer = (state = initialState, action: any) => {
    switch (action.type) {
        case messagesActionTypes.addMessage:    
            const list = [...state.list];
            const item = action.payload;            
            list.push(item);
            const newState = { ...state, list: [...list] };
            return newState;         
        default:
            return state;
    }
};
export default messagesReducer;