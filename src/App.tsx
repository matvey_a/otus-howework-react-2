import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Link, Route, Routes } from 'react-router-dom';
import Home from './components/Home';
import Page404 from './components/404';
import Login from './components/Login';
import Register from './components/Register';
import ChatRoom from './components/Chat/ChatRoom';
import ChatMain from './components/Chat/ChatMain';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route index element={<Home />} />
        <Route path="login" element={<Login />} />
        <Route path="page404" element={<Page404 />} />
        <Route path="reg" element={<Register />} />
        <Route path="chat" element={<ChatMain />} />       
        <Route path="*" element={<Page404 />}/>
      </Routes>

      <nav>
        <ul>
          <li>
            <Link to={'/login'}>Cтраница login</Link>
          </li>
          <li>
            <Link to={'/page404'}>Cтраница 404</Link>
          </li>
          <li>
            <Link to={'/reg'}>Cтраница register</Link>
          </li>
          <li>
            <Link to={'/chat'}>Cтраница Chat</Link>
          </li>     
        </ul>
      </nav>

    </BrowserRouter>
  );
}

export default App;
